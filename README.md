# Pacman
 
Build Instructions

Set Visual studio to x86.

In solution properties:

C/C++ -> General -> Additional Include Directions:
$(ProjectDir)glew\include
$(ProjectDir)freeglut\include

C/C++ -> Preprocessor:
GLEW_STATIC

Linker -> General -> Additional Library Directories:
$(ProjectDir)glew\lib
$(ProjectDir)freeglut\lib

Linker -> Input -> Additional Dependencies:
freeglut.lib
glew32s.lib

