#include "Map.h"

Map::Map() : m_mapOffset(16.0f)
{
	m_map = TextureManager::getInstance().loadTexture(GL_TEXTURE_2D, GL_REPEAT, "Assets/map.png", false, 4);
	m_bigPill = TextureManager::getInstance().loadTexture(GL_TEXTURE_2D, GL_REPEAT, "Assets/big_pill.png", false, 4);
	m_smallPill = TextureManager::getInstance().loadTexture(GL_TEXTURE_2D, GL_REPEAT, "Assets/pill.png", false, 4);
}


Map::~Map()
{
}

//Every draw function will need:
/*
glPushMatrix();

translateMapCoords(x, y);

"call drawSprite for some sprite"

glPopMatrix();
*/
void Map::drawMap()
{
	glPushMatrix();

	//translate the map to the center of the screen
	translateMapOrigin();

	//draw the background map
	TextureManager::getInstance().drawSprite(m_map, 448, 496, 0);

	//draw the different objects in there respective tiles
	for (int x = 0; x < 28; x++) {

		glPushMatrix();
		for (int y = 0; y < 31; y++) {

			switch (getTile(x, y))
			{
			case O:
				TextureManager::getInstance().drawSprite(m_bigPill, 16, 16, 0);
				break;
			case o:
				TextureManager::getInstance().drawSprite(m_smallPill, 16, 16, 0);
				break;
			}
			translateMapToCoords(0, 1);
		}
		glPopMatrix();

		translateMapToCoords(1, 0);
	}
	glPopMatrix();

}

//translate object to maps origin (bottom left corner)
void Map::translateMapOrigin()
{
	glTranslatef(76.0f, 52.0f, 0.0f);
}

//translates a point to a location on the game map
void Map::translateMapToCoords(float x, float y)
{
	glTranslatef(x * m_mapOffset, y * m_mapOffset, 0.0f);
}

bool Map::checkIfWall(TileType type)
{
	if (type == W || type == G)
		return true;

	return false;
}

TileType Map::getTile(int x, int y)
{
	return map[x][y];
}
