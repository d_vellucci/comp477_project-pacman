#define STB_IMAGE_IMPLEMENTATION
#include "TextureManager.h"
#include "stb_image.h"

uint TextureManager::loadTexture(GLenum target, GLenum wrapping, const char* texturePath, bool flip, int channels)
{
	uint tex_handle = 0;
	glGenTextures(1, &tex_handle);
	glBindTexture(GL_TEXTURE_2D, tex_handle); // all upcoming GL_TEXTURE_2D operations now have effect on this texture object

	// so we can blend it 
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	// set the texture wrapping parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// set texture wrapping to GL_REPEAT (default wrapping method)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// set texture filtering parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	int width, height, nrChannels;

	unsigned char* data = stbi_load(texturePath, &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
	}

	stbi_image_free(data);

	return tex_handle;
}

void TextureManager::drawSprite(unsigned int texture, int length, int height, float angle)
{
	// Begin new transformation matrix
	glPushMatrix();
	//rgb(255, 255, 255);   // Reset drawing colour to white, preventing texture discolouration

	int halfLength = length / 2;
	int halfHeight = height / 2;

	// Translate to center of sprite and rotate if necessary
	glTranslatef((float)halfLength, (float)halfHeight, 0.0f);
	glRotatef(angle, 0.0f, 0.0f, 1.0f);

	// Enable texturing and bind selected sprite
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);

	// Draw sprite shape as square to be textured of size (length,height)
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.0f);      // Bottom left
	glVertex2i(-halfLength, -halfHeight);
	glTexCoord2f(1.0f, 0.0f);      // Bottom right
	glVertex2i(halfLength, -halfHeight);
	glTexCoord2f(1.0f, 1.0f);      // Top right
	glVertex2i(halfLength, halfHeight);
	glTexCoord2f(0.0f, 1.0f);      // Top left
	glVertex2i(-halfLength, halfHeight);
	glEnd();

	glDisable(GL_TEXTURE_2D);

	// Pop matrix to ignore above transformations on future objects
	glPopMatrix();
}

void TextureManager::rgb(float r, float g, float b)
{
	glColor3f(r / 255, g / 255, b / 255);
}
